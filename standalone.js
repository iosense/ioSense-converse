/**
 * ioSense Converse standalone
 *
 * @author Jacky Bourgeois
 */

// === === === Read config file === === ===

const fs = require('fs');
const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

// Setting the logs
const log4js = require('log4js');
const logger = log4js.getLogger('[ioSense-converse]');
logger.level = config.logger.level;

const express = require('express');
const app = express();

const converse = require('./index');

app.use(converse);

const http = require('http').Server(app);

const https = require('https');
let credentials = {};
if (config.converse.protocol === 'https') {
    credentials = {
        key: fs.readFileSync(config.ssl.key),
        cert: fs.readFileSync(config.ssl.pem),
        requestCert: true,
        rejectUnauthorized: false,
    };
}

/**
 * Publish the server.
 */
const message = 'ioSense Developer Portal is listening on port : ';
if (config.converse.protocol === 'https') {
    https.createServer(credentials, app)
        .listen(config.converse.port, () => {
        logger.info(message + config.converse.port + ' (https)');
    });
} else {
    http.listen(config.converse.port, () => {
        logger.info(message + config.converse.port + ' (http)');
    });
}
