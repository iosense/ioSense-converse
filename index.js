/**
 * ioSense Converse endpoint
 *
 * @author Jacky Bourgeois
 */

// === === === Read config file === === ===

const fs = require('fs');
const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

// Setting the logs
const log4js = require('log4js');
const logger = log4js.getLogger('[ioSense-converse]');
logger.level = config.logger.level;

const prpl = require('prpl-server');

const httpsClient = require('https');
const httpClient = require('http');

const express = require('express');
const path = require('path');
const app = module.exports = express();

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: '50mb'}));

app.post('/:namespace/login', (req, res) => {
    const namespace = req.params.namespace;
    let username = req.body.username;
    let password = req.body.password;
    if (username !== undefined && password !== undefined) {
        const params = {
            grant_type: 'password',
            client_id: config.converse.clientId + '_' + namespace,
            client_secret: config.converse.secret,
            username: username + '_' + namespace,
            password: password,
        };
        const body = [];
        for (let p in params) {
            if (params.hasOwnProperty(p)) {
                body.push(encodeURIComponent(p)
                    + '=' + encodeURIComponent(params[p]));
            }
        }
        const strBody = body.join('&');

        send('/' + namespace + '/oauth/token', 'POST', '', strBody,
            'application/x-www-form-urlencoded', (error, token) => {
                if (error) {
                    return res.status(403).json(
                        {error: {code: 0, message: 'Access denied.'}});
                } else {
                    return res.status(200).json(token);
                }
            });
    } else {
        return res.status(403).json(
            {error: {code: 0, message: 'Missing username or password'}});
    }
});

if (config.converse.local) {
    app.use(express.static(path.join(__dirname, '.'),
        {etag: false, maxAge: 100}));
    const pages = ['', 'converse-home', 'converse-things', 'converse-error404'];
    pages.forEach((page) => {
        app.get('/' + page, (req, res) => {
            res.render('home', {});
        });
    });
} else {
    app.get('/*', prpl.makeHandler('build', {
        builds: [
            {
                name: 'es6-unbundled',
                basePath: true,
                browserCapabilities: ['es2015', 'push'],
            },
            {
                name: 'es6-bundled',
                basePath: true,
                browserCapabilities: ['es2015'],
            },
            {name: 'es5-bundled', basePath: true},
        ],
    }));
    app.get('/images/*', express.static(
      path.join(__dirname + 'images', 'images/'),
        {etag: false, maxAge: 100}));
}


/**
 * Send HTTP requests.
 * @param {string} endpoint
 * @param {string} method
 * @param {string} token
 * @param {Object} body
 * @param {string} contentType
 * @param {function} callback
 */
function send(endpoint, method, token, body, contentType, callback) {
    let bodyStr = '';
    if (body !== null) {
        if (typeof body !== 'string' && !(body instanceof String)) {
            bodyStr = JSON.stringify(body);
        } else {
            bodyStr = body;
        }
    }

    const header = {
        'Content-Type': contentType,
        'Content-Length': Buffer.byteLength(bodyStr),
    };

    if (token !== '') {
        header.Authorization = token;
    }

    // An object of options to indicate where to post to
    const options = {
        host: config.host,
        port: config.port,
        path: endpoint,
        method: method,
        headers: header,
    };

    // Set up the request
    let message = '';
    let postReq;
    if (config.protocol === 'https') {
        postReq = httpsClient.request(options, (res) => {
            res.setEncoding('utf8');
            res.on('error', (error) => {
                logger.info('error: ' + error);
            });
            res.on('data', (chunk) => {
                message += chunk;
            });
            res.on('end', () => {
                try {
                    const jsonData = JSON.parse(message);
                    callback(null, jsonData);
                } catch (err) {
                    callback({
                        code: 0, message:
                        'Unable to parse the response as JSON for endpoint '
                        + endpoint + '\nResponse:\n' + message,
                    });
                }
            });
        });
    } else {
        postReq = httpClient.request(options, (res) => {
            res.setEncoding('utf8');
            res.on('error', (err) => {
                logger.info('error: ' + err);
            });
            res.on('data', (chunk) => {
                message += chunk;
            });
            res.on('end', () => {
                try {
                    const jsonData = JSON.parse(message);
                    callback(null, jsonData);
                } catch (err) {
                    callback({
                        code: 0, message:
                        'Unable to parse the response as JSON for endpoint '
                        + endpoint + '\nResponse:\n' + message,
                    });
                }
            });
        });
    }

    // post the data
    postReq.write(bodyStr);
    postReq.end();
}
